// Now we actually got a calender :)
// At this point we need to convert this to a visual calender, and take user arguments
var dayNames = new Array(7);
    dayNames[0] = "Sø";
    dayNames[1] = "Ma";
    dayNames[2] = "Ti";
    dayNames[3] = "On";
    dayNames[4] = "To";
    dayNames[5] = "Fr";
    dayNames[6] = "Lø";

var monthNames = new Array(12);
    monthNames[0] = "Januar";
    monthNames[1] = "Februar";
    monthNames[2] = "Marts";
    monthNames[3] = "April";
    monthNames[4] = "Maj";
    monthNames[5] = "Juni";
    monthNames[6] = "Juli";
    monthNames[7] = "August";
    monthNames[8] = "September";
    monthNames[9] = "Oktober";
    monthNames[10] = "November";
    monthNames[11] = "December";
    

$(document).ready(function() {
    
    //Stroring all days picked
    var daysPicked = [];
    
    var monthPicked = new Date();

    // monthPicked should be dynamic, byt the users choice (but only the year and month, the day shoul always be 1)        
    monthPicked.setDate(1);
    renderDefaulCalender(monthPicked);

    $('<div id="monthDisplay">'+monthNames[monthPicked.getMonth()]+'</div>').appendTo('#calenderNavigationWrapper');
    
    //Function to set month up by 1
    $('#calenderNextMonthButton').click(function(){
                        
        var monthCount = monthPicked.getMonth();
        monthCount = monthCount + 1;
        monthPicked.setMonth(monthCount);
        $('#monthDisplay').html(''+monthNames[monthPicked.getMonth()]+'');
        $('#calender').empty();
        $('#calender').html(renderDefaulCalender(monthPicked));
                        
        //Hightligting already picked days
        highlightPickedDays(daysPicked);
            
    });
    
    //Setting month 1 back
    $('#calenderPrevMonthButton').click(function(){
                                
        var monthCount = monthPicked.getMonth();
        monthCount = monthCount - 1;
        monthPicked.setMonth(monthCount);
        $('#monthDisplay').html(''+monthNames[monthPicked.getMonth()]+'');
        $('#calender').empty();
        $('#calender').html(renderDefaulCalender(monthPicked));
        
        //Hightligting already picked days
        highlightPickedDays(daysPicked);
            
    });

    $(document).on('click', '.day', function() {
        
        //Checking if button already are active or not.
        if($(this).hasClass("active")) {
            $(this).removeClass("active"); 
            var existingIndex = $.inArray($(this).attr('id'), daysPicked);  
            daysPicked.splice(existingIndex,1);
        } else {
            $(this).addClass("active");
            var date = $(this).attr('id');
            daysPicked.push(date);
        }
                
    });

});

