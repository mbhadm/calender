//Add the moment this is just for backup

function renderDefaulCalender(monthPicked) {

var firstDay = new Date(monthPicked.getFullYear(), monthPicked.getMonth(), monthPicked.getDate());

// 0 will give the last day of the past month, thats why we need to + the month by 1 so we get back to current.
var lastDay = new Date(monthPicked.getFullYear(), monthPicked.getMonth() +1, 0);

$.each(dayNames, function(index, value) {
    $('<div class="dayNames">'+value+'</div>').appendTo('#calender');
    //Breaking af day names display
    if(index === 6) {
        $('<br>').appendTo('#calender');
    }
});

// Getting the blanks that should be placed in front when the 1 of the month is not sunday
$.each(dayNames, function(index) {
   if(index < firstDay.getDay()) {
       $('<div class="emptyDay"></div>').appendTo('#calender');
   } 
});

// Getting all the day names in the month
for(var i = firstDay.getDate(); i <= lastDay.getDate(); i++) {
    var day = new Date(monthPicked.getFullYear(), monthPicked.getMonth(), i);
    
    $.each(dayNames, function(index){
        if(day.getDay() === index) {         
            $('<div class="day" id="'+day.getFullYear()+'/'+(day.getMonth() +1)+'/'+day.getDate()+'">'+day.getDate()+'</div>').appendTo('#calender');
            
            //Breaking on every week
            if(day.getDay() === 6) {
                $('<br>').appendTo('#calender');
            }
        }
    });
    
}

// Setting blanks at the end until we hit a full week.
$.each(dayNames, function(index) {
   if(index > lastDay.getDay()) {
       $('<div class="emptyDay"></div>').appendTo('#calender');
   } 
});
}

function highlightPickedDays(daysPicked) {
    $(daysPicked).each(function(index, value) {
       day = document.getElementById(value);
       $(day).addClass('active');
    });
}




